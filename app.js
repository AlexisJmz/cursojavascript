document.getElementById("app").innerHTML = "javascript";
let app = document.getElementById("app");

//let name = prompt('what´s your name?');
//let age = prompt('what´s your age?');
//app.innerHTML = `welcome ${name} of ${age} age`

let mensaje, tecnologia;
const PI = 3.1416;
mensaje = "Aprendiendo javascript, css html";
tecnologia = " PHP, JAVA, C#, LARAVEL";
console.log(mensaje, tecnologia);
console.log(mensaje.length);
console.log(tecnologia.substring(0, 11));
console.log(tecnologia.slice(0, 3));
console.log(tecnologia.slice(-10));
console.log(mensaje.split(" "));
console.log(mensaje.indexOf("html"));
console.log(mensaje.replace("javascript", "java"));
console.log(mensaje.repeat(10));
console.log(tecnologia.includes("C#"));
console.log(mensaje.toUpperCase());
console.log(mensaje.toLowerCase());

//numeros

const n1 = 2,
  n2 = 49.59,
  n3 = 500;

let resultado = 0;
//add
resultado = n1 + n2;
//remove
resultado = n3 - n2;
//multiplication
resultado = n1 * n3;
//divisition
resultado = n3 / n1;
//modulo
resultado = n3 % 2;

//operation
//mix
resultado = Math.min(44, 66, 2, 66);
//max
resultado = Math.max(50, 30, 500, 33, 22);
//random
resultado = Math.random();
//potencia
resultado = Math.pow(8, 3);
//PI
resultado = Math.PI;
//REDONDEO
resultado = Math.round(n2);
resultado = Math.floor(n2);
resultado = Math.ceil(n2);
//Raiz cuadrada
resultado = Math.sqrt(155);
//absoluto
resultado = Math.abs(-40);

console.log(resultado);

// typeof

let num;
num = 0;

num = Symbol("simbolo");
num = [1, 2, 3, 4];
num = {
  nombre: "alexhiz",
  empleo: "desarrollador de software",
};
num = new Date();
console.log(typeof num);

//convertir string a number

let v;

v = "20";
v = Number(v);

v = "30";
v = parseInt(v);

v = "60.605";
v = Number(v);
v = parseFloat(v);

// para usar cantidad de digitos despues del . se usa toFixed(?)
v = v.toFixed(1);
console.log(v);

//convertir numero a strings

let cp = 20303;

cp = String(cp);
cp = cp.toString();
console.log(cp);
console.log(cp.length);

//templates

let html = `<ul>
						<li>holaa</li>
						
					</ul>`;

console.log(html);
